import * as defaultTheme from 'tailwindcss/defaultTheme'

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    container: {
      center: true,
      padding: '1rem',
      screens: {
        DEFAULT: '100%',
        lg: '1520px',
      }
    },
    extend: {
      fontFamily: {
        sans: ['Work Sans', ...defaultTheme.fontFamily.sans],
        lato: ['Lato', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        primary: {
          DEFAULT: '#1C1D2C',
          white: '#FBFBFB'
        },
        secondary: {
          DEFAULT: '#12142D',
        },
        gray: {
          DEFAULT: '#575757',
          1: '#D8D8D8'
        },
        accent: {
          DEFAULT: '#72C899',
          orange: '#FF9C30'
        },
        system: {
          3: '#EEF7FD'
        }
      }
    }
  },
  plugins: [],
}
