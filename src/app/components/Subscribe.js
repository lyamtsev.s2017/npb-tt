import Image from 'next/image'
import Button from '@/components/Button.js'

const savings = [
  {
    src: '/img/Subscribe/beauty.svg',
    alt: 'beauty',
    title: 'Save',
    description: 'Save HUGE on Nitric Oxide Flow every mouth'
  },
  {
    src: '/img/Subscribe/gift.svg',
    alt: 'gift',
    title: 'Free Gifts',
    description: 'Exclusive and free surprise gifts'
  },
  {
    src: '/img/Subscribe/calendar.svg',
    alt: 'calendar',
    title: 'On Your Schedule',
    description: 'Modify, pause, cancel any time'
  },
  {
    src: '/img/Subscribe/vitamin.svg',
    alt: 'vitamin',
    title: 'Nitric Oxide Flow For Life',
    description: 'Protected from TikTok sell-outs'
  },
]

export default function Subscribe() {
  return (
    <div className={'container'}>
      <section className={'flex flex-col items-center'}>
        <h2 className={'mb-10 text-3xl font-extrabold leading-7 text-center'}>Subscribe & Save</h2>
        <section className={'grid gap-y-10 mb-10'}>
          {savings.map((saving) => (
            <div key={saving.alt} className={'flex flex-col items-center'}>
              <Image src={saving.src} alt={saving.alt} width={80} height={80} className={'mb-5'} />
              <span className={'max-w-[311px] text-center mb-2.5 text-lg font-medium leading-[1.2]'}>{saving.title}</span>
              <p className={'max-w-[311px] text-center text-[18px]/[1.2]'}>{saving.description}</p>
            </div>
          ))}
        </section>
        <Button
          tag={'a'}
          href="#"
          target={'_blank'}
          className="mx-4 w-full max-w-[311px]"
        >
          Try it now - risk free
        </Button>
      </section>
    </div>
  )
}
