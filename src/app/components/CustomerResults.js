import Image from 'next/image'

const reviews = [
  {
    videoPlaceholder: '/img/CustomerResults/man-video-placeholder.png',
    title: 'My blood pressure has dropped',
    rating: 5,
    text: "It was pretty good. My blood pressure has dropped and I've been feeling pretty good. I got more energy too.",
    name: 'Dwight C.',
  },
  {
    videoPlaceholder: '/img/CustomerResults/bp-video-placeholder.png',
    title: 'This has really worked for me',
    rating: 5,
    text: 'I was really happy that the day after I started taking this, my BP dropped into the “green” range. 120/60 has been something I have been unable to achieve in years on various BP meds. Now I achieve numbers like that regularly. I do not yet know what combination of meds and supplements will be the final solution though I have no doubt this will be an important part of it. Thank you!',
    name: 'David B.',
  },
]

export default function CustomerResults() {
  return (
    <div className={'container mb-20'}>
      <section>
        <h2></h2>

        <main className={'grid gap-y-10 lg:gap-y-20 mb-10'}>
          {reviews.map((review, idx) => (
            <section
              key={idx}
              className={
                'grid gap-y-6 grid-cols-1 lg:gap-x-10 lg:grid-cols-[760px_1fr] lg:odd:grid-cols-[1fr_760px]'
              }
            >
              <section
                className={
                  'relative mx-auto w-full max-w-[311px] h-[204px] lg:max-w-none lg:h-[500px] lg:mx-0'
                }
              >
                <Image
                  src={review.videoPlaceholder}
                  alt={'video placeholder'}
                  fill
                />
              </section>
              <section>
                <h3
                  className={'mb-2.5 font-bold text-xl leading-5 text-center'}
                >
                  &quot;{review.title}&quot;
                </h3>
                <section className={'mb-5 flex justify-center gap-x-1'}>
                  {Array(review.rating)
                    .fill(0)
                    .map((_, idx) => (
                      <Image
                        key={idx}
                        src={'/img/CustomerResults/star.svg'}
                        alt={'star'}
                        width={20}
                        height={20}
                      />
                    ))}
                </section>
                <section className={'flex flex-col items-center'}>
                  <p
                    className={
                      'text-center text-lg leading-6 max-w-[311px] mb-5'
                    }
                  >
                    “{review.text}“
                  </p>
                  <span className={'text-gray font-medium'}>
                    —{review.name}
                  </span>
                </section>
              </section>
            </section>
          ))}
        </main>

        <footer>
          <h3 className={'mb-5 text-center text-2xl font-extrabold'}>
            As seen on
          </h3>
          <section
            className={
              'flex flex-wrap mx-auto items-center justify-center max-w-[311px] lg:max-w-none lg:mx-0'
            }
          >
            <a
              href={'#'}
              target={'_blank'}
              className={'w-[75px] h-[22px] lg:w-[232px] lg:h-[71px] relative'}
            >
              <Image
                src={'/img/CustomerResults/amazon.png'}
                alt={'amazon'}
                fill
              />
            </a>
            <span className={'mx-2 h-9 w-px bg-gray-1'}></span>
            <a
              href={'#'}
              target={'_blank'}
              className={'w-[95px] h-[37px] lg:w-[305px] lg:h-[116px] relative'}
            >
              <Image
                src={'/img/CustomerResults/walmart.png'}
                alt={'walmart'}
                fill
              />
            </a>
            <span className={'mx-2 h-9 w-px bg-gray-1'}></span>
            <a
              href={'#'}
              target={'_blank'}
              className={'w-[81px] h-[24px] lg:w-[265px] lg:h-[77px] relative'}
            >
              <Image
                src={'/img/CustomerResults/tiktok.png'}
                alt={'tiktok'}
                fill
              />
            </a>
            <span className={'mx-2 h-9 w-px bg-gray-1 hidden'}></span>
            <a
              href={'#'}
              target={'_blank'}
              className={'w-[82px] h-[46px] lg:w-[294px] lg:h-[165px] relative'}
            >
              <Image src={'/img/CustomerResults/meta.png'} alt={'meta'} fill />
            </a>
            <span className={'mx-2 h-9 w-px bg-gray-1'}></span>
            <a
              href={'#'}
              target={'_blank'}
              className={'w-[58px] h-[19px] lg:w-[208px] lg:h-[71px] relative'}
            >
              <Image
                src={'/img/CustomerResults/google.png'}
                alt={'google'}
                fill
              />
            </a>
          </section>
        </footer>
      </section>
    </div>
  )
}
