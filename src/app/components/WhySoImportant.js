import Image from 'next/image'

const reasons = [
  {
    image: '/img/WhySoImportant/helps-boosts.jpg',
    alt: 'helps-boosts',
    title: 'Helps Boosts Overall Circulation',
    text: 'Higher nitrate intake helps improve overall circulation flow.',
  },
  {
    image: '/img/WhySoImportant/supports-pressure.jpg',
    alt: 'supports-pressure',
    title: 'Supports Normal Blood Pressure*',
    text: 'Increasing your nitric oxide product can help support normal blood pressure.',
  },
  {
    image: '/img/WhySoImportant/alleviates-strain.jpg',
    alt: 'alleviates-strain',
    title: 'Alleviates Strain Put On Heart Muscles*',
    text: "With better circulation your heart doesn't need to work as hard as it use too.",
  },
]

export default function WhySoImportant() {
  return (
    <div className={'container mb-20'}>
      <section>
        <h2 className={'text-3xl font-extrabold leading-7 text-center mb-10'}>Why Is Heart Health So Important?</h2>

        <main className={'grid gap-y-5'}>
          {reasons.map((reason, index) => (
            <>
              <section
                className={
                  'relative grid grid-rows-[340px_1fr] bg-white rounded-[20px] shadow-[0_4px_10px_0_rgba(0,0,0,0.2)]'
                }
              >
                <span
                  className={
                    'absolute flex items-center uppercase font-bold pr-4 pb-2 text-[60px] justify-center top-0 left-0 rounded-[20px_40px_400px_40px] bg-white w-[120px] h-[120px] shadow-[0_4px_10px_0_rgba(0,0,0,0.1)] z-10'
                  }
                >{index + 1}</span>
                <header className={'relative h-full'}>
                  <Image src={reason.image} alt={reason.alt} fill className={'rounded-t-[20px]'} />
                </header>
                <main className={'px-4 py-8'}>
                  <h3 className={'text-center mb-2.5 text-[28px]/[normal] font-semibold'}>{reason.title}</h3>
                  <p className={'text-center text-lg font-medium'}>{reason.text}</p>
                </main>
              </section>
              {index !== reasons.length - 1 && (
                <Image
                  className={'mx-auto lg:mx-none lg:my-auto lg:rotate-90'}
                  src={'/img/WhySoImportant/fast-forward.svg'}
                  alt={'next step arrow'}
                  width={55}
                  height={80}
                />
              )}
            </>
          ))}
        </main>
      </section>
    </div>
  )
}
