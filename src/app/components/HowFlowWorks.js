import Image from 'next/image'

export default function HowFlowWorks() {
  return (
    <div className={'bg-system-3 py-10 mb-20'}>
      <div className={'container'}>
        <section className={'grid gap-y-10'}>
          <section>
            <h2
              className={
                'text-center font-extrabold leading-6 text-3xl max-w-[311px] mb-7 lg:max-w-none'
              }
            >
              How Does Nitric Oxide Flow work?
            </h2>
            <ul
              className={
                'list-outside list-disc marker:text-accent pl-6 mb-10 font-extrabold text-xl gap-y-4 grid'
              }
            >
              <li>Supports Blood Pressure*</li>
              <li>Supports healthy heart*</li>
              <li>Improves blood flow*</li>
              <li>Reduces fatigue*</li>
              <li>Improves oxygen intake*</li>
              <li>Helps muscle recovery*</li>
            </ul>
            <section
              className={
                'flex mx-auto justify-around max-w-[350px] lg:max-w-[558px]'
              }
            >
              <div className={'flex flex-col items-center'}>
                <div
                  className={
                    'relative w-[60px] h-[60px] lg:w-[100px] lg:h-[100px]'
                  }
                >
                  <Image
                    src={'/img/HowFlowWorks/vegan.svg'}
                    alt={'vegan'}
                    fill
                  />
                </div>
                <span></span>
              </div>
              <div className={'flex flex-col items-center'}>
                <div
                  className={
                    'relative w-[60px] h-[60px] lg:w-[100px] lg:h-[100px]'
                  }
                >
                  <Image
                    src={'/img/HowFlowWorks/gluten-free.svg'}
                    alt={'gluten-free'}
                    fill
                  />
                </div>
                <span></span>
              </div>
              <div className={'flex flex-col items-center'}>
                <div
                  className={
                    'relative w-[60px] h-[60px] lg:w-[100px] lg:h-[100px]'
                  }
                >
                  <Image
                    src={'/img/HowFlowWorks/non-gmo.svg'}
                    alt={'non-gmo'}
                    fill
                  />
                </div>
                <span></span>
              </div>
              <div className={'flex flex-col items-center'}>
                <div
                  className={
                    'relative w-[60px] h-[60px] lg:w-[100px] lg:h-[100px]'
                  }
                >
                  <Image
                    src={'/img/HowFlowWorks/all-natural.svg'}
                    alt={'all-natural'}
                    fill
                  />
                </div>
                <span></span>
              </div>
            </section>
          </section>
          <section>
            <section
              className={
                'mx-auto relative max-w-[383px] w-full h-[375px] lg:h-[700px] lg:mx-0 lg:max-w-[714px]'
              }
            >
              <Image
                src={'/img/HowFlowWorks/nitrio-oxide-flow.png'}
                alt={'nitrio oxide flow'}
                fill
              />
            </section>

            <section className={'grid gap-y-5'}>
              <section>
                <header className={'mb-5 grid grid-cols-[30px_1fr] gap-x-2.5'}>
                  <Image
                    src={'/img/HowFlowWorks/blood-pressure.svg'}
                    alt={'blood pressure'}
                    width={30}
                    height={30}
                  />
                  <h3 className={'font-extrabold text-2xl uppercase'}>
                    Blood Pressure Support
                  </h3>
                </header>
                <p className={'font-medium text-black'}>
                  Helps support blood pressure by increasing your overall nitric
                  oxide levels and circulation.
                  <a href={'#'} target={'_blank'}>
                    12†
                  </a>
                </p>
              </section>
              <section>
                <header className={'mb-5 grid grid-cols-[30px_1fr] gap-x-2.5'}>
                  <Image
                    src={'/img/HowFlowWorks/veins.svg'}
                    alt={'veins'}
                    width={30}
                    height={30}
                  />
                  <h3 className={'font-extrabold text-2xl uppercase'}>
                    Circulation Support
                  </h3>
                </header>
                <p className={'font-medium text-black'}>
                  Helps increases blood flow, and circulation throughout the
                  body by helping you produce more nitric oxide levels in your
                  bloodstream.
                  <a href={'#'} target={'_blank'}>
                    13†,14†
                  </a>
                </p>
              </section>
              <section>
                <header className={'mb-5 grid grid-cols-[30px_1fr] gap-x-2.5'}>
                  <Image
                    src={'/img/HowFlowWorks/timer.svg'}
                    alt={'timer'}
                    width={30}
                    height={30}
                  />
                  <h3 className={'font-extrabold text-2xl uppercase'}>
                    Fast Recovery
                  </h3>
                </header>
                <p className={'font-medium text-black'}>
                  Helps deliver powerful nutrients through better circulation
                  and blood flow. This may also help increase energy and
                  stamina.
                  <a href={'#'} target={'_blank'}>
                    15†
                  </a>
                </p>
              </section>
            </section>
          </section>
        </section>
      </div>
    </div>
  )
}
