'use client'

import Image from 'next/image'
import { useState } from 'react'
import Button from '@/components/Button.js'

const featureImages = [
  {
    src: '/img/Header/30-day.png',
    alt: '30 days',
  },
  {
    src: '/img/Header/made-in-usa.png',
    alt: 'made in usa',
  },
  {
    src: '/img/Header/gmp.png',
    alt: 'good manufacturing practice',
  },
  {
    src: '/img/Header/fda.png',
    alt: 'fda registered',
  },
]

const slides = [
  '/img/Header/pills.png',
  '/img/Header/supplement-facts.png',
  '/img/Header/pills.png',
  '/img/Header/supplement-facts.png',
  '/img/Header/pills.png',
  '/img/Header/supplement-facts.png',
]

export default function Header() {
  const [currentSlideIdx, setCurrentSlideIdx] = useState(0)

  return (
    <div className="container mb-20">
      <header>
        <h1 className="flex flex-col items-center font-extrabold text-center text-[32px]/[0.97] mb-10 lg:text-[70px]/[82px] lg:mb-20">
          Support Your Blood Pressure With
          <strong className="text-accent">Nitric Oxide Flow</strong>
        </h1>

        <section className="grid lg:grid-cols-[500px_1fr] xl:grid-cols-[710px_1fr] lg:gap-x-10">
          <section>
            <section className="grid lg:grid-cols-[80px_1fr] lg:mb-[70px]">
              <section
                className="
          grid grid-cols-[repeat(4,40px)] grid-rows-[40px] justify-center gap-5 mb-5
          lg:grid-cols-1 lg:grid-rows-none lg:auto-rows-[80px] lg:mb-0 lg:content-center
"
              >
                {featureImages.map((image) => (
                  <Image
                    key={image.alt}
                    src={image.src}
                    alt={image.alt}
                    width={80}
                    height={80}
                  />
                ))}
              </section>

              <section className="w-full max-h-[554px] mb-5 mx-auto">
                <Image
                  src="/img/header/npb-pills.png"
                  alt="npb-pills"
                  className="object-contain"
                  width={584}
                  height={554}
                />
              </section>
            </section>

            <section className="flex flex-nowrap overflow-x-auto mb-5">
              {slides.map((slideImg, slideIdx) => (
                <button
                  key={slideIdx}
                  onClick={() => setCurrentSlideIdx(slideIdx)}
                  className={`
                  relative border-2 mr-2 last:mr-0 rounded min-w-[50px] flex-shrink-0 flex-grow-0 h-[50px] lg:min-w-[100px] lg:h-[100px] lg:rounded-[10px]
                  lg:mr-5
                  ${
                    currentSlideIdx === slideIdx
                      ? 'border-accent-orange'
                      : 'border-transparent'
                  }
                  `}
                >
                  <Image src={slideImg} alt="pills" fill />
                </button>
              ))}
            </section>
          </section>

          <section className="flex flex-col">
            <p className="mb-5 text-[28px]/[1.1] px-4 lg:mb-10">
              Helps restore blood flow and support normal blood pressure
            </p>
            <ul className="list-checkmark grid gap-y-6 text-2xl p-4 pl-10 mb-10 bg-system-3 rounded-[20px] lg:p-10 lg:pl-[70px]">
              <li>
                Helps support normal blood pressure by increasing your nitric
                oxide production.
              </li>
              <li>
                Helps increase your overall heart healthy energy without a crash
                by supplying your body with more nitrates.
              </li>
              <li>
                Helps improve overall physical performance and muscle recovery.
              </li>
              <li>
                Helps support your cardiovascular system and boost your immune
                function.
              </li>
            </ul>

            <Button
              tag={'a'}
              href="#"
              target={'_blank'}
              className="mx-4 w-full max-w-[400px]"
            >
              Try it now - risk free
            </Button>
          </section>
        </section>
      </header>
    </div>
  )
}
