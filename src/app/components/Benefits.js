import Image from 'next/image'

export default function Benefits() {
  return (
    <div className={'bg-[url(/img/Benefits/npb-bg.jpg)] bg-no-repeat bg-center bg-cover py-10 mb-20'}>
      <div className={'container'}>
        <section>
          <h2 className={'mb-10 text-3xl font-extrabold leading-6 text-center'}>Nitric Oxide Benefits</h2>
          <section className={'grid gap-y-6 justify-end'}>
            <section className={'grid grid-rows-[80px_1fr] gap-y-5 justify-items-center'}>
              <div className={'relative h-full w-20'}>
                <Image src={'/img/Benefits/varicose-veins.svg'} alt={'varicose veins'} fill />
              </div>
              <span className={'font-semibold text-lg max-w-[140px] text- text-center'}>Restores Blood Vessels*</span>
            </section>
            <section className={'grid grid-rows-[80px_1fr] gap-y-5 justify-items-center'}>
              <div className={'relative h-full w-20'}>
                <Image src={'/img/Benefits/veins.svg'} alt={'veins'} fill />
              </div>
              <span className={'font-semibold text-lg max-w-[140px] text- text-center'}>Restores Circulation*</span>
            </section>
            <section className={'grid grid-rows-[80px_1fr] gap-y-5 justify-items-center'}>
              <div className={'relative h-full w-20'}>
                <Image src={'/img/Benefits/blood-pressure-meter.svg'} alt={'pressure meter'} fill />
              </div>
              <span className={'font-semibold text-lg max-w-[140px] text- text-center'}>Support Blood Pressure*</span>
            </section>
            <section className={'grid grid-rows-[80px_1fr] gap-y-5 justify-items-center'}>
              <div className={'relative h-full w-20'}>
                <Image src={'/img/Benefits/o2.svg'} alt={'o2'} fill />
              </div>
              <span className={'font-semibold text-lg max-w-[140px] text- text-center'}>Boosts Oxygen Levels*</span>
            </section>
          </section>
        </section>
      </div>
    </div>
  )
}
