import '../styles/globals.css'
import Navbar from '@/components/Navbar.js'
import Footer from '@/components/Footer.js'

export const metadata = {
  title: 'NPB - Main',
  favicon: '/next.svg',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>
        <Navbar />

        <main>{children}</main>

        <Footer />
      </body>
    </html>
  )
}
