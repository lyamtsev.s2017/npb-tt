import Header from '@/app/components/Header.js'
import CustomerResults from '@/app/components/CustomerResults.js'
import HowFlowWorks from '@/app/components/HowFlowWorks.js'
import WhySoImportant from '@/app/components/WhySoImportant.js'
import Benefits from '@/app/components/Benefits.js'
import Subscribe from '@/app/components/Subscribe.js'

export default function Home() {
  return (
    <>
      <Header />
      <CustomerResults />
      <HowFlowWorks />
      <WhySoImportant />
      <Benefits />
      <Subscribe />
    </>
  )
}
