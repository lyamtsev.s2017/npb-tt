export default function Button({ children, className, tag = 'button', ...props }) {
  const ButtonTag = tag.toLowerCase()

  return (
    <ButtonTag
      className={`
      orange-btn bg-accent-orange rounded-[10px] uppercase font-extrabold text-white text-xl px-8 py-5 flex items-center justify-center
      lg:text-2xl mx-4 self-center 
      ${className}`}
      {...props}
    >
      {children}
    </ButtonTag>
  )
}
