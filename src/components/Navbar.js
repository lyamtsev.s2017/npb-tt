import Image from 'next/image'

export default function Navbar() {
  return (
    <div className="mb-5 shadow-[0_4px_40px_0] shadow-black/5 bg-white sticky top-0 lg:mb-10 z-10">
      <div className="container">
        <nav className="py-4 lg:py-10 flex justify-center">
          <a href="#" className="relative w-[100px] lg:w-[144px]">
            <Image width={144} height={48} src="/img/logo.svg" alt="npb logo" />
          </a>
        </nav>
      </div>
    </div>
  )
}
